/*jslint node: true*/
'use strict';

module.exports = {
  profileServicePath: "http://localhost/Focus.Profile.Service",
  JWTSecret: "DEV_SECRET",
  port: "3000",
  mongoSettingsConnection: "mongodb://settingsUser:focusPassword@localhost:27017/settings"
};