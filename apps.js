/*jslint node: true*/
'use strict';

module.exports = [
  {"name": "dummy", "host": "http://lpadummy.herokuapp.com", "root": ""},
  
  {"name": "profile", "host": "http://localhost", "root": "Focus.Profile.App"},
  
  {"name": "Wtm", "host": "http://localhost", "root": "Focus.Wtm.App"},
  {"name": "FedPol", "host": "http://localhost", "root": "Focus.FedPol.App"},
  {"name": "PhotoApp", "host": "http://localhost", "root": "Focus.Photo.App"}
];
