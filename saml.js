/*jslint node: true*/
'use strict';

module.exports = {
  mainIdps: {
    "localhost:3000": "vagrant",
    "127.0.0.1:3000": "vagrant"
  },
  idps: [{
    name: "wso2_idp",
    callback: "/saml_token",
    loginPath: "/saml_login",
    issuer: "http://dev.focus/sp",
    cert: "MIICNTCCAZ6gAwIBAgIES343gjANBgkqhkiG9w0BAQUFADBVMQswCQYDVQQGEwJVUzELMAkGA1UECAwCQ0ExFjAUBgNVBAcMDU1vdW50YWluIFZpZXcxDTALBgNVBAoMBFdTTzIxEjAQBgNVBAMMCWxvY2FsaG9zdDAeFw0xMDAyMTkwNzAyMjZaFw0zNTAyMTMwNzAyMjZaMFUxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJDQTEWMBQGA1UEBwwNTW91bnRhaW4gVmlldzENMAsGA1UECgwEV1NPMjESMBAGA1UEAwwJbG9jYWxob3N0MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCUp/oV1vWc8/TkQSiAvTousMzOM4asB2iltr2QKozni5aVFu818MpOLZIr8LMnTzWllJvvaA5RAAdpbECb+48FjbBe0hseUdN5HpwvnH/DW8ZccGvk53I6Orq7hLCv1ZHtuOCokghz/ATrhyPq+QktMfXnRS4HrKGJTzxaCcU7OQIDAQABoxIwEDAOBgNVHQ8BAf8EBAMCBPAwDQYJKoZIhvcNAQEFBQADgYEAW5wPR7cr1LAdq+IrR44iQlRG5ITCZXY9hI0PygLP2rHANh+PYfTmxbuOnykNGyhM6FjFLbW2uZHQTY1jMrPprjOrmyK5sjJRO4d1DeGHT/YnIjs9JogRKv4XHECwLtIVdAbIdWHEtVZJyMSktcyysFcvuhPQK8Qc/E/Wq8uHSCo=",
    userIdAttributeName: "uid",
    url: "https://192.168.82.19/samlsso",
    privateCert: "./backend/certificates/development.pem",
    acceptedClockSkewMs: 2000
  }, {
    name: "vagrant",
    callback: "/vagrant_saml_token",
    loginPath: "/vagrant_saml_token",
    issuer: "http://dev.focus/sp",
    userIdAttributeName: "uid",
    url: "http://localhost:7000",
    privateCert: "./backend/certificates/development.pem",
    acceptedClockSkewMs: 24 * 60 * 60 * 1000 //a day
  }]
};